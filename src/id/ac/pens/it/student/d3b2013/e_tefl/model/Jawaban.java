package id.ac.pens.it.student.d3b2013.e_tefl.model;

public class Jawaban {
	public Jawaban(int id, String text) {
		super();
		this.id = id;
		this.text = text;
	}

	private int id;
	private String text;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}