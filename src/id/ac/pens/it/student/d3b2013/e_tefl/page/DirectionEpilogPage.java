package id.ac.pens.it.student.d3b2013.e_tefl.page;

import id.ac.pens.it.student.d3b2013.e_tefl.model.Group;

public class DirectionEpilogPage extends ListeningPage {

	private Group epilog;
	
	public DirectionEpilogPage(Group direction,Group epilog) {
		super(direction,0);		
		this.epilog = epilog;
	}	
	
	public Group getEpilog() {
		return epilog;
	}
	
	public Group getDirection(){
		return getGroup();
	}

	@Override
	public String getPage() {
		return "Direction";
	}
	
	
}
