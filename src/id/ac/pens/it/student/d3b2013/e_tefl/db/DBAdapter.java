package id.ac.pens.it.student.d3b2013.e_tefl.db;

import id.ac.pens.it.student.d3b2013.e_tefl.model.Group;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Jawaban;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Soal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter extends SQLiteOpenHelper {

	private static DBAdapter instance;

	public static DBAdapter with(Context context) {
		if (instance == null)
			instance = new DBAdapter(context);
		return instance;
	}

	private static final int VERSION = 1;

	private static Context context;

	public DBAdapter(Context context) {
		super(context, "etoefl", null, VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			Scanner s = new Scanner(context.getAssets().open("main.sql"));
			StringBuilder text = new StringBuilder();
			while (s.hasNextLine()) {
				text.append(s.nextLine() + "\n");
			}

			String[] sqls = text.toString().split(";");

			for (String sql : sqls) {
				try {
					db.execSQL(sql);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public List<Soal> getStructure() {
		return getStructure(10);
	}

	public List<Soal> getStructure(int count) {
		List<Soal> soals = new ArrayList<Soal>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from soal where id_group=0 and type=\"text\" order by random() limit " + count, null);
		if (c.moveToFirst()) {
			do {
				soals.add(new Soal(c.getInt(0), c.getString(1), getJawabans(c.getInt(0)), c.getInt(2),c.getString(4).equals("text")));
			} while (c.moveToNext());
		}
		db.close();

		return soals;
	}

	public Jawaban getJawaban(int id) {
		Jawaban jawaban = null;
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from jawaban where id_soal=\"" + id + "\"", null);
		if (c.moveToFirst()) {
			do {
				jawaban = (new Jawaban(c.getInt(0), c.getString(1)));
			} while (c.moveToNext());
		}
		c.close();
		db.close();

		return jawaban;
	}

	public List<Jawaban> getJawabans(int id) {
		List<Jawaban> jawaban = new ArrayList<Jawaban>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from jawaban where id_soal=\"" + id + "\" order by random()", null);
		if (c.moveToFirst()) {
			do {
				jawaban.add(new Jawaban(c.getInt(0), c.getString(1)));
			} while (c.moveToNext());
		}
		c.close();
		db.close();

		return jawaban;
	}

	public List<Soal> getSoal(int group) {
		List<Soal> soals = new ArrayList<Soal>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from soal where id_group=" + group + " order by random() limit 10", null);
		if (c.moveToFirst()) {
			do {
				soals.add(new Soal(c.getInt(0), c.getString(1), getJawabans(c.getInt(0)), c.getInt(2),c.getString(4).equals("text")));
			} while (c.moveToNext());
		}
		db.close();

		return soals;
	}

	public List<Group> getReading() {
		return getReading(2);
	}

	public List<Group> getReading(int count) {
		List<Group> result = new ArrayList<Group>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from kelompok where type=0 order by random() limit " + count, null);
		if (c.moveToFirst()) {
			do {
				result.add(new Group(c.getInt(0), c.getString(1), getSoal(c.getInt(0))));
			} while (c.moveToNext());
		}
		db.close();
		return result;
	}

	public List<Soal> getListeningA() {
		List<Soal> soals = new ArrayList<Soal>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from soal where id_group=0 and type=\"audio\" order by random() limit 5", null);
		if (c.moveToFirst()) {
			do {
				soals.add(new Soal(c.getInt(0), c.getString(1), getJawabans(c.getInt(0)), c.getInt(2),c.getString(4).equals("text")));
			} while (c.moveToNext());
		}
		db.close();

		return soals;

	}

	public List<Group> getListening(int type) {
		List<Group> result = new ArrayList<Group>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from kelompok where type="+type+" order by random() limit 1", null);
		if (c.moveToFirst()) {
			do {
				result.add(new Group(c.getInt(0), c.getString(1), getSoal(c.getInt(0))));
			} while (c.moveToNext());
		}
		db.close();
		return result;
	}

	public Group getDirection(int part) {
		List<Group> result = new ArrayList<Group>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from kelompok where type=" + (-part) + " limit 1", null);
		if (c.moveToFirst()) {
			do {
				result.add(new Group(c.getInt(0), c.getString(1), getSoal(c.getInt(0))));
			} while (c.moveToNext());
		}
		db.close();
		return result.size()>0? result.get(0) : null;
	}

	public Group getEpilog(int part) {
		List<Group> result = new ArrayList<Group>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("select * from kelompok where type=" + (-part) + "0 limit 1", null);
		if (c.moveToFirst()) {
			do {
				result.add(new Group(c.getInt(0), c.getString(1), getSoal(c.getInt(0))));
			} while (c.moveToNext());
		}
		db.close();
		return result.size()>0? result.get(0) : null;
	}

}