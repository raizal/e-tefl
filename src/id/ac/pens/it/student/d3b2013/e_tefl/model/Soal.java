package id.ac.pens.it.student.d3b2013.e_tefl.model;

import java.util.List;

public class Soal {

	public Soal(int id, String soal, List<Jawaban> jawabans, int jawaban) {
		super();
		this.id = id;
		this.soal = soal;
		this.group = group;
		this.jawabans = jawabans;
		this.jawaban = jawaban;
	}
	
	public Soal(int id, String soal, List<Jawaban> jawabans, int jawaban, boolean isText) {
		super();
		this.id = id;
		this.soal = soal;
		this.group = group;
		this.jawabans = jawabans;
		this.jawaban = jawaban;
		this.isText = isText;
	}

	private int id;
	private String soal;
	private Group group;
	private List<Jawaban> jawabans;
	private int jawaban;
	private boolean isText = true;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSoal() {
		return soal;
	}

	public void setSoal(String soal) {
		this.soal = soal;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public List<Jawaban> getJawabans() {
		return jawabans;
	}

	public void setJawabans(List<Jawaban> jawabans) {
		this.jawabans = jawabans;
	}

	public int getJawaban() {
		return jawaban;
	}

	public void setJawaban(int jawaban) {
		this.jawaban = jawaban;
	}
	
	public boolean isText() {
		return isText;
	}

}
