package id.ac.pens.it.student.d3b2013.e_tefl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageButton;

public class BelajarActivity extends ActionBarActivity {

	private ImageButton reading;
	private ImageButton listening;
	private ImageButton structure;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.belajar);
		
		reading = (ImageButton) this.findViewById(R.id.reading);
		reading.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), LearnActivity.class);
				i.putExtra("title", "Reading");
				startActivity(i);
			}
		});
		structure = (ImageButton) this.findViewById(R.id.structure);
		structure.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), LearnActivity.class);
				i.putExtra("title", "Structure");
				startActivity(i);
			}
		});
		
		listening = (ImageButton) this.findViewById(R.id.listening);
		listening.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), LearnActivity.class);
				i.putExtra("title", "Listening");
				startActivity(i);
			}
		});
		
	}
}
