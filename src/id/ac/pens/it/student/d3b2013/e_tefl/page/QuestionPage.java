package id.ac.pens.it.student.d3b2013.e_tefl.page;

import id.ac.pens.it.student.d3b2013.e_tefl.R;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Jawaban;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Soal;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuestionPage extends Page {

	private Soal soal;
	
	public int selected = -1;
	
	private View v;
	
	public QuestionPage(Soal soal, int page) {
		super(page);
		this.soal = soal;		
	}

	public boolean isRight(){
		return selected == soal.getJawaban();
	}
	
	public Soal getSoal() {
		return soal;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		v = inflater.inflate(R.layout.course_learn_item, container, false);
		TextView text = (TextView) v.findViewById(R.id.soal);
		
		if(soal.isText())
			text.setText(soal.getSoal());
		else
			text.setText("");
		
		int i = 1;
		for (final Jawaban j : soal.getJawabans()) {							
			int resId = getResources().getIdentifier("rb" + i, "id", inflater.getContext().getPackageName());			
			((RadioButton)v.findViewById(resId)).setText(j.getText());
			
			((RadioButton)v.findViewById(resId)).setOnClickListener(new OnClickListener() {				
				@Override
				public void onClick(View v) {
					if(((RadioButton)v).isChecked()){
						selected = j.getId();
					}
				}
			});
			
			i++;
		}
		return v;
	}

	
	public void disableOptions(){
		((RadioGroup)v.findViewById(R.id.rg)).setEnabled(false);		
	}
}
