package id.ac.pens.it.student.d3b2013.e_tefl;




import id.ac.pens.it.student.d3b2013.e_tefl.db.DBAdapter;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

@SuppressLint({ "NewApi", "HandlerLeak" })

public class MainActivity extends Activity {
	ImageButton mulai;
	ImageButton about;
	ImageButton help;
	ImageButton score;
	
	private int[] LEAVES = { 
			R.drawable.green_leaf, 
			R.drawable.green_leaf,
			R.drawable.green_leaf,
			R.drawable.green_leaf,
		};

	private Rect mDisplaySize = new Rect();
	
	private RelativeLayout mRootLayout;
	private ArrayList<View> mAllImageViews = new ArrayList<View>();
	
	private float mScale;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);
		
		DBAdapter.with(this).getWritableDatabase();
		
		Display display = getWindowManager().getDefaultDisplay(); 
		display.getRectSize(mDisplaySize);
		
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		mScale = metrics.density;

		mRootLayout = (RelativeLayout) findViewById(R.id.main_layout);

		new Timer().schedule(new ExeTimerTask(), 0, 5000);
		
		ImageButton mulai = (ImageButton)this.findViewById(R.id.imgMulai);
		mulai.setOnClickListener(new View.OnClickListener(){
			@Override
			
			public void onClick(View v){
				Intent i = new Intent (getApplicationContext(), MenuJalur.class);
				startActivity(i);
			}
				});
		ImageButton about = (ImageButton)this.findViewById(R.id.imgAbout);
		about.setOnClickListener(new View.OnClickListener(){
			@Override
			
			public void onClick(View v){
				Intent in = new Intent (getApplicationContext(),About.class);
				startActivity(in);
			}
				});
		ImageButton help = (ImageButton)this.findViewById(R.id.imgHelp);
		help.setOnClickListener(new View.OnClickListener(){
			@Override
			
			public void onClick(View v){
				Intent inte = new Intent (getApplicationContext(),Help.class);
				startActivity(inte);
			}
				});
		ImageButton score = (ImageButton)this.findViewById(R.id.imgScore);
		score.setOnClickListener(new View.OnClickListener(){
			@Override
			
			public void onClick(View v){
				Intent inten = new Intent (getApplicationContext(),Score.class);
				startActivity(inten);
			}
				});
		
		
	}

	@SuppressLint("NewApi")
	public void startAnimation(final ImageView aniView) {

		aniView.setPivotX(aniView.getWidth()/2);
		aniView.setPivotY(aniView.getHeight()/2);

		long delay = new Random().nextInt(Constant.MAX_DELAY);

		final ValueAnimator animator = ValueAnimator.ofFloat(0, 1);
		animator.setDuration(Constant.ANIM_DURATION);
		animator.setInterpolator(new AccelerateInterpolator());
		animator.setStartDelay(delay);

		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			
			int angle = 50 + (int)(Math.random() * 101);
			int movex = new Random().nextInt(mDisplaySize.right);
			
			@SuppressLint("NewApi")
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				float value = ((Float) (animation.getAnimatedValue())).floatValue();
				
				aniView.setRotation(angle*value);
				aniView.setTranslationX((movex-40)*value);
				aniView.setTranslationY((mDisplaySize.bottom + (150*mScale))*value);
			}
		});

		animator.start();
	}
	
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			int viewId = new Random().nextInt(LEAVES.length);
			Drawable d = getResources().getDrawable(LEAVES[viewId]);
			LayoutInflater inflate = LayoutInflater.from(MainActivity.this);
			ImageView imageView = (ImageView) inflate.inflate(R.layout.ani_image_view, null);
			imageView.setImageDrawable(d);
			mRootLayout.addView(imageView);
			
			mAllImageViews.add(imageView);			
			
			LayoutParams animationLayout = (LayoutParams) imageView.getLayoutParams();
			animationLayout.setMargins(0, (int)(-150*mScale), 0, 0);
			animationLayout.width = (int) (60*mScale);
			animationLayout.height = (int) (60*mScale);
			
			startAnimation(imageView);
		}
	};
	
	private class ExeTimerTask extends TimerTask {
		@Override
		public void run() {
			// we don't really use the message 'what' but we have to specify something.
			mHandler.sendEmptyMessage(Constant.EMPTY_MESSAGE_WHAT);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}