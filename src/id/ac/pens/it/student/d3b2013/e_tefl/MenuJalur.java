package id.ac.pens.it.student.d3b2013.e_tefl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageButton;

public class MenuJalur extends ActionBarActivity {
	private ImageButton belajar;
	private ImageButton kursus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);

		belajar = (ImageButton) this.findViewById(R.id.imgBelajar);
		belajar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), BelajarActivity.class);
				startActivity(i);
			}
		});
		kursus = (ImageButton) this.findViewById(R.id.imgKursus);
		kursus.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(), CourseActivity.class);
				startActivity(i);
			}
		});
	}
}
