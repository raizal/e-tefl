package id.ac.pens.it.student.d3b2013.e_tefl.model;

import java.util.List;

public class Group {
		
	public Group(int id, String text, List<Soal> soal, Type type) {
		super();
		this.id = id;
		this.text = text;
		this.soal = soal;
		this.type = type;
	}

	public Group(int id, String text, List<Soal> soal) {
		super();
		this.id = id;
		this.text = text;
		this.soal = soal;
	}	
	
	private int id;
	private String text;
	private List<Soal> soal;

	public enum Type {
		READING, LISTENING
	};

	private Type type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<Soal> getSoal() {
		return soal;
	}

	public void setSoal(List<Soal> soal) {
		this.soal = soal;
	}

}
