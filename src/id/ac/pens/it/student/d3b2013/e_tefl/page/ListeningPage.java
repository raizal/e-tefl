package id.ac.pens.it.student.d3b2013.e_tefl.page;

import id.ac.pens.it.student.d3b2013.e_tefl.R;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Group;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ListeningPage extends Page {

	private Group group;
	private ImageView play;

	private boolean isPlay = false;

	public ListeningPage(Group group, int page) {
		super(page);
		this.group = group;
	}

	public Group getGroup() {
		return group;
	}

	@Override
	public String getPage() {
		return "Listening " + super.getPage();
	}

	public void setStatus(boolean play) {
		isPlay = play;
		if (this.play != null) {
			if (isPlay)
				this.play.setImageResource(android.R.drawable.ic_media_pause);
			else
				this.play.setImageResource(android.R.drawable.ic_media_play);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.listening_item, container, false);

		play = (ImageView) v.findViewById(R.id.play);

		if (isPlay)
			this.play.setImageResource(android.R.drawable.ic_media_pause);
		else
			this.play.setImageResource(android.R.drawable.ic_media_play);
		return v;
	}

}
