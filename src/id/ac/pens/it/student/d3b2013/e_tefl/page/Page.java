package id.ac.pens.it.student.d3b2013.e_tefl.page;

import android.support.v4.app.Fragment;

public class Page extends Fragment {
	
	public Page(int page) {
		this.page = page;
	}
	
	private int page;

	public String getPage() {
		return page+"";
	}
}
