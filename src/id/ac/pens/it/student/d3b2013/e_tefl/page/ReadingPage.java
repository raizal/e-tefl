package id.ac.pens.it.student.d3b2013.e_tefl.page;

import id.ac.pens.it.student.d3b2013.e_tefl.R;
import id.ac.pens.it.student.d3b2013.e_tefl.R.id;
import id.ac.pens.it.student.d3b2013.e_tefl.R.layout;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Group;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ReadingPage extends Page {

	private Group group;

	public ReadingPage(Group group,int page) {
		super(page);
		this.group = group;
	}

	@Override
	public String getPage() {
		return "Reading "+super.getPage();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.reading_item, container, false);
		TextView text = (TextView) v.findViewById(R.id.text);
		text.setText(group.getText().replaceAll("\\n","\n"));
		return v;
	}

}
