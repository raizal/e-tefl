package id.ac.pens.it.student.d3b2013.e_tefl.page;

import id.ac.pens.it.student.d3b2013.e_tefl.R;
import id.ac.pens.it.student.d3b2013.e_tefl.R.id;
import id.ac.pens.it.student.d3b2013.e_tefl.R.layout;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ScorePage extends Page {

	private int salah, benar;

	public ScorePage(int salah, int benar) {
		super(salah);
		this.salah = salah;
		this.benar = benar;
	}

	@Override
	public String getPage() {
		return "Score";
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.score, container, false);
		TextView textSalah = (TextView) v.findViewById(R.id.salah), textBenar = (TextView) v.findViewById(R.id.benar), textScore = (TextView) v.findViewById(R.id.score);
		textSalah.setText("Salah : "+salah);
		textBenar.setText("Benar : "+benar);
		int score = (int) (benar / (float)(salah+benar)*100);
		textScore.setText("Score : "+score);
		return v;
	}

}
