package id.ac.pens.it.student.d3b2013.e_tefl;

import id.ac.pens.it.student.d3b2013.e_tefl.db.DBAdapter;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Group;
import id.ac.pens.it.student.d3b2013.e_tefl.model.Soal;
import id.ac.pens.it.student.d3b2013.e_tefl.page.DirectionEpilogPage;
import id.ac.pens.it.student.d3b2013.e_tefl.page.ListeningPage;
import id.ac.pens.it.student.d3b2013.e_tefl.page.Page;
import id.ac.pens.it.student.d3b2013.e_tefl.page.QuestionLearnPage;
import id.ac.pens.it.student.d3b2013.e_tefl.page.QuestionPage;
import id.ac.pens.it.student.d3b2013.e_tefl.page.ReadingPage;
import id.ac.pens.it.student.d3b2013.e_tefl.page.ScorePage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

public class LearnActivity extends ActionBarActivity implements OnCompletionListener, OnPageChangeListener {

	private ViewPager viewpager;
	private int page = 0;
	private List<Page> items = new ArrayList<Page>();
	private long start = 0, end = 0;
	private boolean finish = false;

	private int currentPlay = -1;
	private MediaPlayer mp = new MediaPlayer();

	private boolean epilog = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.course_view);
		mp.setOnCompletionListener(this);
		Intent intent = getIntent();
		String title = intent.getStringExtra("title").toLowerCase();
		getSupportActionBar().setTitle(intent.getStringExtra("title"));

		viewpager = (ViewPager) findViewById(R.id.viewpager);

		int page = 1;

		if (title.toLowerCase().equals("structure")) {
			for (Soal s : DBAdapter.with(this).getStructure(5)) {
				items.add(new QuestionLearnPage(s, page++));
			}
		} else if (title.toLowerCase().equals("reading")) {

			int reading = 1;
			for (Group g : DBAdapter.with(this).getReading(1)) {
				items.add(new ReadingPage(g, reading++));
				for (Soal s : g.getSoal()) {
					items.add(new QuestionLearnPage(s, page++));
				}
			}
		} else {
			// Part A
			for (Soal s : DBAdapter.with(this).getListeningA()) {
				items.add(new QuestionLearnPage(s, page++));
			}

			// Part B
			items.add(new DirectionEpilogPage(DBAdapter.with(this).getDirection(2), null));

			for (Group g : DBAdapter.with(this).getListening(2)) {
				items.add(new ListeningPage(g, page++));
				for (Soal s : g.getSoal()) {
					items.add(new QuestionLearnPage(s, page++));
				}
			}
			// Part C
			items.add(new DirectionEpilogPage(DBAdapter.with(this).getDirection(3), DBAdapter.with(this).getEpilog(3)));

			for (Group g : DBAdapter.with(this).getListening(3)) {
				items.add(new ListeningPage(g, page++));
				for (Soal s : g.getSoal()) {
					items.add(new QuestionLearnPage(s, page++));
				}
			}
		}

		viewpager.setAdapter(new Adapter(items));
		viewpager.setOnPageChangeListener(this);
		start = System.currentTimeMillis();
		onPageSelected(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (page == items.size() - 1 && !finish) {

		} else if (page < items.size() - 1)
			getMenuInflater().inflate(R.menu.course, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.finish) {
			finish = true;
			int salah = 0;
			int benar = 0;
			for (Page p : items) {
				if (p instanceof QuestionPage) {
					if (!((QuestionPage) p).isRight())
						salah++;
					else
						benar++;
					((QuestionPage) p).disableOptions();
				}
			}
			end = System.currentTimeMillis();
			items.add(new ScorePage(salah, benar));
			((Adapter) viewpager.getAdapter()).notifyDataSetChanged();
			viewpager.setCurrentItem(items.size() - 1, true);
			supportInvalidateOptionsMenu();
		} else if (id == R.id.next) {
			viewpager.setCurrentItem(++page, true);
		} else if (id == R.id.ulang) {
			startActivity(new Intent(this, CourseActivity.class));
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	private class Adapter extends FragmentPagerAdapter {
		private List<Page> items;

		public Adapter(List<Page> items) {
			super(getSupportFragmentManager());
			this.items = items;
		}

		@Override
		public Fragment getItem(int arg0) {
			return items.get(arg0);
		}

		@Override
		public int getCount() {
			return items.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			if (position < items.size())
				return items.get(position).getPage();
			else
				return "";
		}

	}

	@Override
	protected void onDestroy() {
		try {
			mp.release();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int arg0) {
		LearnActivity.this.page = arg0;

		Page page = items.get(arg0);
		if (currentPlay == -1 && !mp.isPlaying()) {
			if (page instanceof ListeningPage) {
				currentPlay = arg0;
				ListeningPage listeningPage = (ListeningPage) page;

				AssetFileDescriptor afd;
				try {
					afd = getResources().getAssets().openFd(listeningPage.getGroup().getText());
					mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
					mp.prepare();
					listeningPage.setStatus(true);
					mp.start();
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else if (page instanceof QuestionLearnPage) {
				currentPlay = arg0;
				QuestionLearnPage qPage = (QuestionLearnPage) page;
				if (!qPage.getSoal().isText()) {
					AssetFileDescriptor afd;
					try {
						afd = getResources().getAssets().openFd(qPage.getSoal().getSoal());
						mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
						mp.prepare();
						mp.start();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else if (page instanceof DirectionEpilogPage) {
				epilog = false;
				currentPlay = arg0;
				DirectionEpilogPage dPage = (DirectionEpilogPage) page;
				AssetFileDescriptor afd;
				try {
					afd = getResources().getAssets().openFd(dPage.getDirection().getText());
					mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
					mp.prepare();
					dPage.setStatus(true);
					mp.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (arg0 == items.size() - 1) {
			supportInvalidateOptionsMenu();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {

		try {
			mp.reset();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Page page = items.get(currentPlay);
		if (page instanceof DirectionEpilogPage) {
			DirectionEpilogPage dPage = (DirectionEpilogPage) page;
			if (!epilog && dPage.getEpilog() != null) {
				AssetFileDescriptor afd;
				try {
					epilog = true;
					afd = getResources().getAssets().openFd(dPage.getEpilog().getText());
					mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
					mp.prepare();
					dPage.setStatus(true);
					mp.start();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				dPage.setStatus(false);
				int p = currentPlay + 1;
				currentPlay = -1;
				viewpager.setCurrentItem(p, true);
			}
		} else if (page instanceof ListeningPage) {
			((ListeningPage) page).setStatus(false);
			int p = currentPlay + 1;
			currentPlay = -1;
			viewpager.setCurrentItem(p, true);
		} else if (page instanceof QuestionLearnPage) {
			viewpager.setCurrentItem(currentPlay, true);
			new Handler().postDelayed(new Runnable() {
				public void run() {
					int p = currentPlay + 1;
					currentPlay = -1;
					viewpager.setCurrentItem(p, true);
				}
			}, 5000);
		}
	}

}
