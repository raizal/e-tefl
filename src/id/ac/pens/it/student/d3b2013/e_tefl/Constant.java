package id.ac.pens.it.student.d3b2013.e_tefl;

public class Constant {
	public static final int MAX_DELAY = 6000;
	public static final int ANIM_DURATION = 5000;
	public static final int EMPTY_MESSAGE_WHAT = 0x001;

}
